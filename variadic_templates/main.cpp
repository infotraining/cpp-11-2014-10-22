#include <iostream>
#include <tuple>
#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/algorithm.hpp>

using namespace std;

// ---------------------------------------------------------------------------------------------------------------------

template <typename... Types>
struct Count;

template <typename Head, typename... Tail>
struct Count<Head, Tail...>
{
    constexpr static int value = 1 + Count<Tail...>::value;
};

template <>
struct Count<>
{
    constexpr static int value = 0;
};

template <typename... Types>
struct VerifyCount
{
    static_assert(Count<Types...>::value == sizeof...(Types), "Error in counting number of parameters");
};


// ---------------------------------------------------------------------------------------------------------------------

void print()
{}

template <typename T, typename... Tail>
void print(const T& arg1, Tail... params)
{
    cout << arg1 << endl;
    print(params...);
}


// ---------------------------------------------------------------------------------------------------------------------

void call(int& x)
{
    cout << "call(int&: " << x++ << ")" << endl;
}

void call(const int& x)
{
    cout << "call(const int&: " << x << ")" << endl;
}

void call(int&& x)
{
    cout << "call(int&&: " << x << ")" << endl;
}

template <typename... Args>
void fwd_call(Args&&... params)
{
    call(forward<Args>(params)...);
}

// ---------------------------------------------------------------------------------------------------------------------

// nested parameter packs
template<typename... Args1>
struct zip
{
    template<typename... Args2>
    struct with
    {
        using type = tuple<pair<Args1, Args2>...>;
    };
};

template <size_t... Ns>
struct Indexes
{};

template<typename... Ts, size_t... Ns>
auto cherry_pick(const tuple<Ts...>& t, Indexes<Ns...>) -> decltype(make_tuple(get<Ns>(t)...))
{
    return make_tuple(get<Ns>(t)...);
}

int main()
{
    static_assert(Count<int, double, string&>::value == 3, "must be 3");
    VerifyCount<int, double> {};

    print(1, 5.6, "Hello");

    int x = 10;
    const int cx = 20;

    fwd_call(x);
    fwd_call(cx);
    fwd_call(42);

    zip<int, double, char>::with<string, int, double>::type zipped;

    get<0>(zipped) = make_pair(1, "Hello");
    get<1>(zipped) = make_pair(3.14, 5);
    get<2>(zipped) = make_pair('s', 5.55);

    boost::fusion::for_each(zipped, [](const auto& p) { cout << "[" << p.first << ", " << p.second << "] "; });
    cout << endl;


    auto t1 = make_tuple(5, 4, 5.44, "Hello"s);

    auto picked = cherry_pick(t1, Indexes<0, 3>{});

    boost::fusion::for_each(picked, [](const auto& p) { cout << p << " "; });
    cout << endl;

}

