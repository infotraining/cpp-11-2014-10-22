#include <iostream>
#include <vector>
#include <iterator>
#include <memory>
#include <iterator>
#include <algorithm>

using namespace std;

//template <typename Container>
//auto find_null(Container& container) -> decltype(begin(container))
//{
//    auto it = begin(container);

//    for(; it != end(container); ++it)
//        if (*it == nullptr)
//            break;

//    return it;
//}

template <typename Container>
auto find_null(Container&& container) -> decltype(begin(container))
{
    return find_if(begin(container), end(container),
                   [](decltype(*begin(container))& ptr) { return ptr == nullptr; });
}

int main()
{

    // find_null: Given a container of pointers, return an
    // iterator to the first null pointer (or the end
    // iterator if none is found)

    int x = 10;
    int y = 20;

    int* tab[] = { &x, nullptr, &y };

    cout << distance(begin(tab), find_null(tab)) << endl;

    vector<int*> vec = { NULL, &x, nullptr };

    cout << distance(begin(vec), find_null(vec)) << endl;

    vector<shared_ptr<int>> svec = { make_shared<int>(9), nullptr, shared_ptr<int>{}, make_shared<int>(10) };

    cout << distance(begin(svec), find_null(svec)) << endl;
}
