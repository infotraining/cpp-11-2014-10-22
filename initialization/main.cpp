#include <iostream>
#include <vector>
#include <map>
#include <list>

using namespace std;

struct Point
{
    int x, y;
};

class Vector2D
{
    int x_, y_;
public:
    Vector2D(int x = 0, int y = 0) : x_ {x}, y_ {y}
    {}

    int x() const
    {
        return x_;
    }

    int y() const
    {
        return y_;
    }

    static Vector2D versor_x()
    {
        return {1, 0};
    }

    static Vector2D versor_y()
    {
        return Vector2D {0, 1};
    }
};

ostream& operator<<(ostream& out, const Vector2D& vec)
{
    out << "(" << vec.x() << ", " << vec.y() << ")";
    return out;
}

template <typename Container>
void print(const Container& cont, const string& prefix)
{
    cout <<  prefix << ": [ ";
    for(const auto& item : cont)
        cout << item << " ";
    cout << "]\n";
}

class LVec
{
    list<Vector2D> lst_;
public:
    LVec() : lst_ { Vector2D::versor_x(), {0, 1} }
    {
        cout << "LVec()" << endl;
    }

    LVec(int size) : lst_(size)
    {
        cout << "LVec(int)" << endl;
    }

    LVec(initializer_list<Vector2D> il) : lst_ {il}
    {
        cout << "LVec(initializer_list<Vector2D>)" << endl;
    }

    void show(const string& prefix)
    {
        print(lst_, prefix);
    }
};

int main()
{
    int x1 {};

    cout << "x1: " << x1 << endl;

    int x2 {2};

    cout << "x2: " << x2 << endl;

    auto x3 = int {5};

    cout << "x3: " << x3 << endl;

    auto x4 = int {static_cast<int>(3.4)};

    cout << "x4: " << x4 << endl;

    auto pt1 = Point {1, 3};
    Point pt2 = {1, 4};

    int tab1[] = { 1, 2, 3, static_cast<int>(4.7), 5, 6 };

    Vector2D vec1 {1, 2};
    cout << "vec1: " << vec1 << endl;

    auto vec2 = Vector2D {1, 7};
    cout << "vec2: " << vec2 << endl;

    vector<Vector2D> vvec1 { Vector2D {1, 4}, {5, 6} };

    print(vvec1, "vvec1");

    vector<Vector2D> vvec2 = { Vector2D {7, 9}, {7, 8}, Vector2D {79, 7}, Vector2D::versor_y() };

    print(vvec2, "vvec2");

    LVec lvec {};

    lvec.show("lvec");

    list<vector<int>> lst_vec = { {1, 2, 3, 4}, {7, 6, 5, 2} };

    print(lst_vec.front(), "front of lst_vec");
    print(lst_vec.back(), "back of lst_vec");

    LVec lvec2 = { Vector2D {1, 2}, Vector2D {5, 8}, {7, 22} };
    lvec2.show("lvec2");

    vector<int> vec3{10, -1};
    print(vec3, "{vec3");

    LVec lvec3 {3};
    lvec3.show("lvec3");

    vector<string> words {5, "hello"};
    print(words, "words");

    auto x = { []()->void{} };

    (*x.begin())();
}
