#include <iostream>
#include <thread>
#include <memory>

using namespace std;

auto foo(int n)
{
    if (n % 2 == 0)
        return "even"s;

    return string {"odd"};
}

int main()
{
    this_thread::sleep_for(chrono::milliseconds(1000));
    this_thread::sleep_for(1000ms);

    cout << foo(7) << endl;
}
