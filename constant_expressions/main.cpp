#include <iostream>
#include <utility>
#include <array>

using namespace std;

constexpr int factorial(int n)
{
    return (n == 0) ? 1 : n * factorial(n-1);
}

template <typename T, size_t N>
constexpr size_t size_of_array(T (&)[N])
{
    return N;
}

template <typename T>
constexpr auto square(const T& val) -> decltype(val * val)
{
    return val * val;
}


// TODO: Napisz funkcję fibonacci() obliczającą w czasie kompilacji wartości ciągu fibonacciego
constexpr long fibonacci(int n)
{
    return n < 1 ? -1 : (n == 1 || n == 2 ? 1 : fibonacci(n -1) + fibonacci(n -2));
};

enum class Fibonacci
{
    First = fibonacci(1),
    Ninth = fibonacci(9),
    Tenth = fibonacci(10)
};

class Complex
{
    double real_, imaginary_;
public:
    constexpr Complex(const double& real, const double& imaginary) : real_ {real}, imaginary_ {imaginary}
    {}

    constexpr double real() const { return real_; };
    constexpr double imaginary() const { return imaginary_; }
};

constexpr Complex operator+(const Complex& lhs, const Complex& rhs)
{
    return Complex(lhs.real() + rhs.real(), lhs.imaginary() + rhs.imaginary());
}

// TODO: Napisz klasę będącą tablicą typu T i rozmiaru N, która jest typem literalnym
// i może być użyta w constexpr

template <class ElementType, int N>
class ConstArray
{
    const ElementType a[N];
public:
    template <class... T> constexpr ConstArray(T... p) : a {p...} {}

    constexpr int size() { return N;}

    constexpr ElementType operator[](int i)
    {
        return (i < 0 || i >= N) ? throw "ERROR: out of range": a[i];
    }
};

int main()
{
    constexpr array<int, 3> tab = {1, 2, 3};

    static_assert(tab[0] == 1, "tab[0] != 1");

    const int SIZE = 2;

    int arr1[factorial(1)];
    int arr2[factorial(SIZE)];
    int arr3[factorial(3)];
    int arr4[factorial(size_of_array(arr3))];
    int arr5[square(4)];
    
    
    cout << "n1: " << size_of_array(arr1) << endl;
    cout << "n2: " << size_of_array(arr2) << endl;
    cout << "n3: " << size_of_array(arr3) << endl;
    cout << "n4: " << size_of_array(arr4) << endl;


    // constexpr variables
    constexpr int x = 7;
    constexpr auto prefix = "Data";
    constexpr int n_x = factorial(x);
    constexpr double pi = 3.1415;
    constexpr double pi_2 = pi / 2;

    cout << "x : " << x << endl;
    cout << "prefix : " << prefix << endl;
    cout << "n_x : " << n_x << endl;
    cout << "pi : " << pi << endl;
    cout << "pi_2 : " << pi_2 << endl;

    constexpr Complex c1 {1, 2};
    constexpr Complex c2 {3, 4};

    static_assert((c1 + c2).real() == 4, "Error");
    static_assert((c1 + c2).imaginary() == 6, "Error");

    // TODO:
    cout << "Fibonacci::First = " << static_cast<int>(Fibonacci::First) << endl;
    cout << "Fibonacci::Ninth = " << static_cast<int>(Fibonacci::Ninth) << endl;

    constexpr ConstArray<int, 4> tab1 = { 1, 2, 3, 4 };
    static_assert(tab1[2] == 3, "arr1[2] != 3");
}
