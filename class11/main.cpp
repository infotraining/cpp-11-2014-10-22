#include <iostream>
#include <stdexcept>
#include <memory>
#include <set>

using namespace std;

class Base
{
public:
    virtual void run() = 0;

    virtual ~Base()
    {
        cout << "Base::~Base()" << endl;
    }
};

class Gadget : public Base
{
    string name_ {"unknown"};
public:
    Gadget() : Gadget {"gadget"}
    {}

    Gadget(const string& name) : name_ {name}
    {}

    // kopiujące fcje specjalne
    Gadget(const Gadget&) = default;
    Gadget& operator=(const Gadget&) = default;

    // przenoszące
    Gadget(Gadget&&) = default;
    Gadget& operator=(Gadget&&) = default;

    ~Gadget()
    {
        cout << "Gadget::~Gadget()" << endl;
    }

    void print() const
    {
        cout << "Gadget(name=" << name_ << ")" << endl;
    }

    void run() override final
    {
        cout << "Gadget(" << name_ << ")::run()" << endl;
    }
};

class SuperGadget : public Gadget
{
   using Gadget::Gadget;

//   void run() override
//   {
//       cout << "SuperGadget::run()" << endl;
//   }
};

void may_throw()
{
    throw runtime_error("Error");
}

class Widget
{
    using Data = pair<int, string>;

    int id_;
    string name_;
    unique_ptr<Data> data_;
public:
    Widget(int id, const string& name) : id_{id}, name_ {name}, data_{new Data{id_, name_}}
    {
        //data_ = decltype(data_){new Data{id_, name_}};

        may_throw();

        cout << "Widget(int, string)" << endl;
    }

    Widget(int id) : Widget{id, "unknown"}
    {
        cout << "Widget(int)" << endl;
    }

    Widget() : Widget{-1}
    {
        cout << "Widget()" << endl;
    }

    Widget(const Widget&) = delete;
    Widget& operator=(const Widget&) = delete;

    ~Widget()
    {
        cout << "Widget::~Widget()" << endl;
    }
};

template<typename T, typename Comp = less<T>>
struct IndexableSet : set<T, Comp>
{
    using BaseSet = set<T, Comp>;
    using set<T, Comp>::set;

    IndexableSet() = default;
    IndexableSet(initializer_list<T>) = delete;

    //typename set<T, Comp>::const_reference operator[](size_t index)
    auto operator[](size_t index) -> decltype(*(this->begin()))
    {
        auto it = this->begin();

        advance(it, index);

        return *it;
    }
};


int main() try
{

//    string inspector = "inspector";

//    Gadget g{inspector};
//    g.print();

//    Gadget cg = g;
//    g.print();

//    Gadget mg = move(g);
//    mg.print();

//    //g = Gadget{};
//    cout << "g po move: ";
//    g.print();

//    cout << "\n\n";

    int tab[] = { 1, 2, 3 };

    IndexableSet<int> indexed_set {begin(tab), end(tab)};
    indexed_set.insert({6, 3, 7, 1, 5, 8, 10 });

    cout << indexed_set[0] << " " << indexed_set[1] << endl;

    Widget w{};
}
catch(const exception& e)
{
    cout << e.what() << endl;
}
