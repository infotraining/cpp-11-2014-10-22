#include <iostream>
#include <vector>
#include <map>
#include <functional>
#include <memory>

using namespace std;

template <typename T>
void deduce_type(T t)
{
}

void foo(int x, double y)
{
    cout << "foo(x: " << x << ", y: " << y << ")" << endl;
}

class Gadget
{
    int id_;
public:
    Gadget(int id) : id_{id}
    {}

    int id() const
    {
        return id_;
    }
};

class Base
{
public:
    virtual void run() const { cout << "Base::run()" << endl; }
    virtual ~Base() = default;
};

class Derived : public Base
{
public:
    void run() const override { cout << "Derived::run()" << endl; }
};

double gen()
{
    return 3.14;
}

template <typename Container>
void print(const Container& cont)
{
    cout << "[ ";
//    for(auto it = begin(cont); it != end(cont); ++it)
//        cout << *it << " ";

    for(const auto& item : cont)
        cout << item << " ";
    cout << "]\n";
}

int main()
{
    auto i1 = 42; // i1: int
    auto dx = 3.14; // dx: double

    vector<int> vec1;
    auto vec2 = vector<int> {}; // vec: vector<int>  
    auto it1 = vec2.begin(); // it1: vector<int>::iterator

    map<int, string, greater<int>> dict_desc = { { 1, "one" } };
    auto it2 = dict_desc.begin(); // it2: map<int, string, greater<int>>::iterator

    auto& value = dict_desc[1]; // value: string
    value = "two";
    cout << "dict_desc[1]: " << dict_desc[1] << endl;

    const auto& cvalue = dict_desc[1];  // cvalue: const string&
    //cvalue += "three";

    const auto* ptr1 = &i1; // const int*
    cout << "*ptr1: " << *ptr1 << endl;

    auto ptr2 = &i1; // int*
    cout << "*ptr2: " << *ptr2 << endl;

    const auto* const cptrc = &i1;

    auto sth = { 1, 2, 3, 5, 6 }; // std::initializer_list<int>
    deduce_type(sth);

    auto expr = bind(&foo, placeholders::_1, 3.14);
    expr(1);

    auto l1 = [](int x) { foo(x, 3.14); };
    l1(1);

    cout << typeid(l1).name() << endl;

    auto i2 {8};
    auto i3 = {8};

    auto g1 = Gadget{1};

    auto result = static_cast<int>(gen());

    auto sptr = make_shared<string>("Hello");

    auto ptr_base = shared_ptr<Base>{make_shared<Derived>()};

    ptr_base->run();

    vector<string> words = { "one", "two", "three" };

    for(const auto& w : words)
        cout << w << " ";
    cout << endl;

    for(auto& w : words)
        w += w;

    for(const auto& w : words)
        cout << w << " ";
    cout << endl;

    int tab1[] = {1, 2, 3, 4};

    for(const auto& item : tab1)
        cout << item << " ";
    cout << endl;

    for(const auto& item : {1, 2, 4, 5, 6})
        cout << item << " ";
    cout << endl;

    auto il = { 1, 2, 3, 4 };
    for(const auto& item : il)
        cout << item << " ";
    cout << endl;

    print(words);

    int numbers[] = { 1, 2, 3, 4, 5 };
    print(numbers);

    auto il2 = { 1, 2, 3, 4 };
    print(il2);

    vector<bool> vec_bul = { 0, 1, 1, 1, 0, 0 };

    cout << "vec_bul: ";
    for(auto&& bit : vec_bul)
    {
        bit.flip();
        cout << bit << " ";
    }
    cout << endl;



    return 0;
}
