#include <iostream>
#include <thread>
#include <chrono>
#include <future>
#include <vector>

using namespace std;

int square(int x)
{
    cout << "square(x: " << x << ") starts..." << endl;

    this_thread::sleep_for(chrono::milliseconds(rand() % 5000));

    cout << "square(x: " << x << ") ends..." << endl;
    return x * x;
}

void save_to_file(int x)
{
    cout << "save_to_file: " << x << " starts..." << endl;

    this_thread::sleep_for(chrono::milliseconds(5000));

    cout << "save_to_file: " << x << " ends..." << endl;
}

int main()
{
    auto result = async(launch::async, [] { return square(7);});

    for(int i = 0; i < 5; ++i)
    {
        cout << "Main thread works..." << endl;
        this_thread::sleep_for(100ms);
    }

    cout << "result: " << result.get() << endl;


    vector<int> numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    vector<future<int>> results;

    for(auto& n : numbers)
        results.push_back(async(launch::async, [n] { return square(n);}));

    for(auto& r : results)
        cout << "result: " << r.get() << endl;

    cout << "\n\nProblem with async: ";

    auto f1 = async(launch::async, []{ save_to_file(8);});
    auto f2 = async(launch::async, []{ save_to_file(9);});
}
