#include <iostream>
#include <vector>
#include <set>
#include <functional>
#include <queue>
#include <memory>

using namespace std;

class Lambda_234234
{
public:
    void operator()()
    {
        cout << "Lambda!" << endl;
    }
};

class Lambda_456456
{
    int c1_;
public:
    Lambda_456456(int c1) : c1_ {c1}
    {}

    bool operator()(int arg) const
    {
        return c1_ > arg;
    }
};

class Logger
{
    size_t id_;
public:
    Logger(size_t id) : id_ {id}
    {}

    void log(const string& message)
    {
        cout << "Logger{" << id_ << "}: " << message << endl;
    }

    ~Logger()
    {
        cout << "~Logger(" << id_ << ")" << endl;
    }
};

int main()
{
    auto l1 = [] { cout << "Lambda!" << endl; };

    l1();

    auto add = [](int a, int b) { return a + b; };

    auto result = add(5, 7);
    cout << "result: " << result << endl;

    auto complex_lambda = [](int a, int b, double c) -> double {
                               if ((a > b) && (a > c))
                                   return a;
                               else if ((b > a) && (b > c))
                                   return b;
                               else
                                   return c;
                            };

    auto biggest = complex_lambda(7, 9, 18.1);

    cout << "biggest: " << biggest << endl;

    int x = 10;
    int counter = 0;
    vector<int> vec = { 6, 10, 4324, 123, 34, 123, 1, 9 };

    auto capture1 = [x](int arg) { return x > arg; };

    cout << "10 > 9: " << capture1(9) << endl;

    auto count_gt_x = [x, &counter, &vec] { for(const auto& item : vec) if (item > x) ++counter; };
    count_gt_x();
    cout << "gt_x: " << counter << endl;

    auto comp_ptrs = [](auto ptr1, auto ptr2) { return *ptr1 < *ptr2; };
    set<int*, decltype(comp_ptrs)> set_ptrs {comp_ptrs};
    set<int*, function<bool (int*, int*)>> set_ptrs_alt {comp_ptrs};

    set_ptrs.insert({ new int{5},  new int{1}, new int{10} });

    cout << "set_ptrs: ";
    for(const auto& ptr : set_ptrs)
        cout << *ptr << " ";
    cout << endl;


    queue<function<void()>> jobs;

    Logger logger{1};

    {
        //auto logger = make_shared<Logger>(2);

        auto logger = shared_ptr<Logger> {new Logger{2}};

        jobs.push([logger] { logger->log("Danger call");});
    }

    jobs.push([] { cout << "Start" << endl; });
    jobs.push([&logger] { logger.log("jobs are running"); });

    // later
    while (!jobs.empty())
    {
        auto& f = jobs.front();
        f();

        jobs.pop();
    }

    {
        unique_ptr<Logger> logger3 = unique_ptr<Logger>{new Logger{3}};

        auto make_log = [capt_logger = move(logger3)] { capt_logger->log("Works"); };

//        auto make_log =
//                bind([](unique_ptr<Logger>& logger) { logger->log("Works");}, move(logger3));

        make_log();
    }

    cout << "end of main" << endl;
}
