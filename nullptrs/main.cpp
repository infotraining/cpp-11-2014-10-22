#include <iostream>
#include <memory>

using namespace std;

void foo_ptr(int* ptr)
{
    if (ptr)
        cout << "foo(int*: " << *ptr << ")" << endl;
    else
        cout << "foo(int*: NULL)" << endl;
}

void foo_ptr(long x)
{
    cout << "foo(int: " << x << ")" << endl;
}

void foo_ptr(nullptr_t)
{
    cout << "foo(nullptr)" << endl;
}

int main()
{
    foo_ptr(NULL);

    foo_ptr(static_cast<int*>(NULL));

    foo_ptr(nullptr);

    int* ptr_i1 = nullptr;

    foo_ptr(ptr_i1);

    int* ptr_i2 {}; // inicjalizacja nullptr

    foo_ptr(ptr_i2);

    shared_ptr<int> sptr_i = nullptr;

    if (sptr_i == nullptr)
    {
        cout << "Pusty shared_ptr<int>" << endl;
    }

    if (nullptr == NULL)
        cout << "nullptr == NULL" << endl;

}
