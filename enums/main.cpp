#include <iostream>
#include <map>

using namespace std;

enum Coffee : unsigned char { espresso, cappucino, latte = 255 };

enum class Engine : char { diesel = 0, gasoline = 1, tdi, wankel = 127 };

template <typename Key>
using DictionaryDesc = map<Key, string, greater<Key>>;

int main()
{
    Coffee coffee = espresso;

    int enum_value = espresso;

    cout << "espresso: " << enum_value << endl;

    coffee = static_cast<Coffee>(255);

    if (coffee == latte)
        cout << "latte" << endl;

    Engine e = Engine::gasoline;

    e = static_cast<Engine>(127);

    enum_value = static_cast<int>(Engine::wankel);

    if (e == Engine::wankel)
        cout << "wankel" << endl;

    switch(e)
    {
        case Engine::diesel:
            cout << "diesel\n";
            break;
        case Engine::gasoline:
            cout << "beznyna\n";
            break;
        case Engine::tdi:
            cout << "tdi\n";
            break;
        case Engine::wankel:
            cout << "wankel\n";
            break;
    }

    DictionaryDesc<int> dict1;

    dict1.insert(make_pair(1, "one"));
    dict1.insert({{2, "two"}, {7, "seven"}});

    for(const auto& kv : dict1)
        cout << kv.first << " - " << kv.second << endl;
}
