#include <iostream>
#include <algorithm>
#include <boost/tokenizer.hpp>
#include <iterator>
#include <vector>
#include <type_traits>


using namespace std;

string foo(const string& arg)
{
    cout << "foo(const string&)" << endl;
    return arg + " from foo";
}

void foo(string& arg)
{
    cout << "foo(string&)" << endl;

    arg += " from foo(string&)";
}

string foo(string&& arg)
{
    cout << "foo(string&&)" << endl;

    arg += " from foo(string&&)";

    return arg;
}

vector<string> split(const string& text)
{
    boost::tokenizer<> tok {text};

    vector<string> result { tok.begin(), tok.end() };

    return result;
}

class Brick
{
    string id_ = gen_id();
    size_t size_ {};
    char* bitmap_ {};
public:
    Brick(size_t size) : size_ {size}, bitmap_ {new char[size_]}
    {
        cout << "Brick(size_t: " << size_  << ")" << endl;
        fill_n(bitmap_, size_ - 1, '#');
        bitmap_[size_-1] = 0;
    }

    Brick(const Brick& source)
        : id_ {source.id_}, size_{source.size_}, bitmap_{new char[size_]}
    {
        cout << "Brick(const Brick&)" << endl;
        copy(source.bitmap_, source.bitmap_ + source.size_, bitmap_);
    }

    Brick& operator=(const Brick& source)
    {
        cout << "operator=(Brick&)" << endl;

        if (this != &source)
        {
            string temp_id = source.id_;

            char* temp_bitmap = new char[source.size_];
            copy(source.bitmap_, source.bitmap_ + source.size_, temp_bitmap);

            id_ = move(temp_id);

            delete [] bitmap_;
            bitmap_ = temp_bitmap;

            size_ = source.size_;
        }

        return *this;
    }

    Brick(Brick&& source) noexcept(is_nothrow_move_constructible<string>::value)
        : id_{move(source.id_)}, size_{move(source.size_)}, bitmap_{source.bitmap_}
    {
        cout << "Brick(Brick&&)" << endl;

        source.bitmap_ = nullptr;
    }

    Brick& operator=(Brick&& source) noexcept(is_nothrow_move_assignable<string>::value)
    {
        cout << "operator=(Brick&&)" << endl;

        if (this != &source)
        {
            id_ = move(source.id_);
            size_ = move(source.size_);
            bitmap_ = source.bitmap_;

            source.bitmap_ = nullptr;
        }

        return *this;
    }

    ~Brick()
    {
        delete [] bitmap_;
    }

    void show() const
    {
        cout << id_ << ": [" << (bitmap_ ? bitmap_ : "empty") << "]" << endl;
    }

    static string gen_id()
    {
        static int id;
        return to_string(++id);
    }
};

int main()
{
    string text = "Hello"; // text: lvalue

    foo(text);

    cout << text << endl;

    //string& ref_result = foo(text); // error: can't bind rvalue to lvalue ref

    string&& refref_result = foo(move(text));

    refref_result += " modified";

    cout << refref_result << endl;

    //string&& refref_text = text; // cannot bind lvalue to rvalue ref

    vector<string> words = split("one two three");

    for(auto& w : words)
        cout << w << endl;

    cout<< "\n--------------------\n\n";
    {
        Brick brck1 {10};

        brck1.show();

        Brick cpy_brck1 = brck1; // copy ctor

        Brick mv_brck1 = move(brck1); // move ctor

        brck1 = Brick {30}; // move operator=

        Brick mv_brck2 = move(mv_brck1);
    }

    cout << "\n------------------\n\n";

    vector<Brick> bricks1;
    //bricks.reserve(10);

    Brick brck1 {10};

    bricks1.push_back(brck1);

    bricks1.push_back(move(brck1));

    bricks1.push_back(Brick{20});

    bricks1.emplace_back(30);

    cout << "\nbricks:\n";
    for(auto& b : bricks1)
        b.show();

    cout << "\nmoving with move_iterator:\n";

    auto index = bricks1.size()/2;
    vector<Brick> bricks2 { make_move_iterator(bricks1.begin() + index ), make_move_iterator(bricks1.end()) };
    bricks1.erase(bricks1.begin() + index, bricks1.end());

    cout << "\nbricks:\n";
    for(auto& b : bricks1)
        b.show();

    cout << "\nbricks2:\n";
    for(auto& b : bricks2)
        b.show();


    cout << "\n--------------------------\n\n";
}
