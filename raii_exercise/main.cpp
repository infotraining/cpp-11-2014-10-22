#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <memory>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

class FileGuard
{
    FILE* file_;
public:
    explicit FileGuard(FILE* file) : file_ {file}
    {}

    FileGuard(const FileGuard&) = delete;
    FileGuard& operator=(const FileGuard&) = delete;

    FileGuard(FileGuard&& source) noexcept : file_ {source.file_}
    {
        source.file_ = nullptr;
    }

    FileGuard& operator=(FileGuard&& source) noexcept
    {
        file_ = source.file_;
        source.file_ = nullptr;
    }

    ~FileGuard()
    {
        if (file_)
            fclose(file_);
    }

    explicit operator bool() const
    {
        return file_ != nullptr;
    }

    FILE* get() const
    {
        return file_;
    }
};

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file { fopen(file_name, "w") };

    if (!file)
        throw std::runtime_error("Blad");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
};


void save_to_file_with_up(const char* file_name)
{
    //std::unique_ptr<FILE, decltype(fclose)*> file {fopen(file_name, "w"), &fclose};

    auto custom_dealloc = [](FILE* f) { fclose(f); };
    std::unique_ptr<FILE, decltype(custom_dealloc)> file {fopen(file_name, "w"), custom_dealloc};

    if (!file)
        throw std::runtime_error("Blad");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_sp(const char* file_name)
{
    FILE* cfile = fopen(file_name, "w");

    if (!cfile)
        throw std::runtime_error("Blad");

    std::shared_ptr<FILE> file{ cfile , &fclose };

//    std::shared_ptr<FILE> file_alt{ fopen(file_name, "w") ,
//                                    [](FILE* f) { if (f) fclose(f); }};

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");

    save_to_file_with_up("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
