#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    std::random_device rd;
    std::mt19937 mt {rd()};
    std::uniform_int_distribution<int> uniform_dist {1, 30};

    auto seed = 0;

    auto gen = [seed] () mutable { return ++seed; };


    generate(vec.begin(), vec.end(), gen );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    auto is_even = [](int x) { return x % 2 == 0; };

    // 1a - wyświetl parzyste
    cout << "Evens: ";
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            is_even);
    cout << endl;

    // 1b - wyswietl ile jest parzystych
    cout << "Evens count: " << count_if(vec.begin(), vec.end(), is_even) << endl;


    int eliminators[] = { 3, 5, 7 };
    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    auto garbage_start = remove_if(vec.begin(), vec.end(),
                                  [&eliminators](int x) {
                                      return any_of(begin(eliminators), end(eliminators),
                                                    [x](int arg) { return x % arg == 0; });
                                  });

    vec.erase(garbage_start, vec.end());

    // 3 - tranformacja: podnieś liczby do kwadratu
    //transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x;});
    transform(vec.begin(), vec.end(), vec.begin(), vec.begin(), multiplies<int>());

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());
    cout << "5 the greatest: ";
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, " "));
    cout << endl;

    // 5 - policz wartosc srednia
    // TODO

    auto sum = 0.0;
    for_each(vec.begin(), vec.end(), [&sum] (int x) { sum += x; });

    auto avg = sum / vec.size();

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    vector<int> ls_eq_avg;
    vector<int> gt_avg;

    partition_copy(vec.begin(), vec.end(), back_inserter(ls_eq_avg), back_inserter(gt_avg),
                   [avg](int x) { return x <= avg; });

    cout << "ls_eq_avg: ";
    copy(ls_eq_avg.begin(), ls_eq_avg.end(), ostream_iterator<int>(cout, " "));
    cout << endl;

    cout << "gt_avg: ";
    copy(gt_avg.begin(), gt_avg.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
}
