#include <iostream>
#include <memory>
#include <vector>
#include <cassert>

using namespace std;

class Base
{
public:
    virtual void run() = 0;

    virtual ~Base()
    {
    }
};

class Gadget : public Base
{
    string name_ {"unknown"};
public:
    Gadget() : Gadget {"gadget"}
    {}

    Gadget(const string& name) : name_ {name}
    {
        cout << "Gadget(name: " << name_ << ")" << endl;
    }

    // kopiujące fcje specjalne
    Gadget(const Gadget&) = default;
    Gadget& operator=(const Gadget&) = default;

    // przenoszące
    Gadget(Gadget&&) = default;
    Gadget& operator=(Gadget&&) = default;

    ~Gadget()
    {
        cout << "Gadget::~Gadget(name=" << name_ << ")" << endl;
    }

    void print() const
    {
        cout << "Gadget(name=" << name_ << ")" << endl;
    }

    void run() override final
    {
        cout << "Gadget(" << name_ << ")::run()" << endl;
    }
};

void use(Gadget* g)
{
    if (g)
        g->run();
}

void use_and_destroy(unique_ptr<Gadget> g)
{
    g->run();
}

unique_ptr<Gadget> create_gadget(size_t id)
{
    return unique_ptr<Gadget> {new Gadget{"g" + to_string(id)}};
}

template <typename T, typename... Args>
unique_ptr<T> make_unique(Args&&... args)
{
    return unique_ptr<T>{ new T{forward<Args>(args)...} };
}

int main()
{
    {
        auto ptr1 = make_unique<Gadget>("g1");

        ptr1->run();
        (*ptr1).run();

        ptr1.reset(new Gadget{"g2"});

        ptr1->run();
        use(ptr1.get());

        use_and_destroy(move(ptr1));

        cout << "after use_and_destroy()" << endl;

        auto ptr2 = create_gadget(13);
        auto ptr3 = move(ptr2);

        ptr3->run();

        vector<unique_ptr<Gadget>> gadgets;

        gadgets.push_back(unique_ptr<Gadget>{new Gadget{"i2"}});
        gadgets.push_back(move(ptr3));
        gadgets.push_back(create_gadget(99));
        gadgets.emplace_back(new Gadget{"i3"});

        cout << "\n\nrunning gadgets:\n";

        for(const auto& g : gadgets)
            g->run();

        gadgets[2]->run();

        unique_ptr<Gadget> owner = move(gadgets.back());

        cout << "owner runs: ";
        owner->run();

        assert(gadgets.back() == nullptr);

        cout << "end of scope" << endl;

        shared_ptr<Gadget> shared_gadget1 = move(owner);
        shared_ptr<Gadget> shared_gadget2 = create_gadget(102);
    }

    cout << "\n\nDynamic arrays:\n";

    {
        unique_ptr<Gadget[]> gadgets{new Gadget[10]};

        gadgets[0].run();
    }
}
